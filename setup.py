#!/usr/bin/env python3
"""
This package aims to simplify uploading and downloading of versioned data in cloud storage. It is
specifically aimed at using DigitalOcean Spaces, but can probably be configured to use any
S3/boto3-compliant API.
"""

import os

from setuptools import find_packages, setup

version = "0.1.2"

setup(
    name="data_spaces",
    version=version,
    author="Tor Solli-Nowlan",
    author_email="diag-bioinf@medisin.uio.no",
    description="Simple management of versioned data in S3 cloud storage",
    long_description=__doc__,
    url="https://gitlab.com/ousamg/data-spaces",
    download_url="https://gitlab.com/ousamg/data-spaces/releases",
    license="GPLv3",
    packages=find_packages(),
    include_package_data=True,
    install_requires=["boto3"],
    python_requires=">=3.7",
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Environment :: Web Environment",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Operating System :: MacOS :: MacOS X",
        "Operating System :: POSIX",
        "Operating System :: Unix",
        "Programming Language :: Python",
        "Topic :: Scientific/Engineering",
    ],
)
